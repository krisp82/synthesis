import React from 'react';
import { Route, Switch } from 'react-router-dom';
import HeaderBar from './components/HeaderBar';
import { Content } from 'carbon-components-react';
import Pattern from './content/pattern';
import Detail from './content/detail';
import './App.scss';

function App() {
  return (
    <div className="App">
      <HeaderBar />
      <Content>
        <Switch>
          <Route exact path="/" component={Pattern} />
          <Route path="/detail" component={Detail} />
        </Switch>
      </Content>
    </div>
  );
}

export default App;
