import React, { useState, useEffect } from 'react';
import { Pagination } from 'carbon-components-react';
import PatternTable from '../../components/PatternTable';
import axios from 'axios';

const Pattern = () => {
  const [paging, setPaging] = useState({
    totalItems: 0,
    totalPages: 0,
    currentPage: 1,
    firstRowIndex: 0,
    currentPageSize: 20,
  });
  const [data, setData] = useState([]);

  const calculateTotalPage = total => {
    const isExceedPage = total % paging.currentPageSize === 0 ? 0 : 1;
    const totalPages = total / paging.currentPageSize + isExceedPage;
    return totalPages;
  };

  const queryData = (page, pageSize = paging.currentPageSize) => {
    const result = axios.get(
      `https://5cebb03a77d47900143b8d57.mockapi.io/patterns?page=${page}&limit=${pageSize}`
    );
    result
      .then(result => {
        const { data, total: totalItems } = result.data;
        const totalPages = calculateTotalPage(totalItems);
        setData(data);
        setPaging({ ...paging, totalItems, totalPages });
      })
      .catch(error => {
        console.log(error);
      });
  };

  useEffect(() => {
    queryData(paging.currentPage);
  }, []);
  const headerData = [
    {
      header: 'Product',
      key: 'product',
      size: 12,
    },
    {
      header: 'Size',
      key: 'size',
      size: 8,
    },
    {
      header: 'Status',
      key: 'status',
      size: 8,
    },
    {
      header: 'Notes',
      key: 'notes',
      size: 12,
    },
    {
      header: 'Date created',
      key: 'dateCreated',
      size: 13,
    },
    {
      header: '',
      key: 'detail',
      size: 8,
    },
    {
      header: '',
      key: 'menu',
      size: 3,
    },
  ];

  return (
    <div className="bx--grid bx--grid--narrow pattern-page">
      <div className="bx--row pattern-table__row">
        <div className="bx--col-lg-16">
          <PatternTable rowsData={data} headersData={headerData} />
          <Pagination
            className="pattern--pagination"
            totalItems={paging.totalItems}
            pageSize={paging.currentPageSize}
            page={paging.currentPage}
            isLastPage={paging.currentPage === paging.totalPages}
            pageSizes={[5, 10, 15, 20]}
            onChange={({ page, pageSize }) => {
              queryData(page, pageSize);
            }}
          />
        </div>
      </div>
    </div>
  );
};
export default Pattern;
