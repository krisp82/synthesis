import React from 'react';
import {
  Header,
  HeaderNavigation,
  HeaderMenuItem,
  HeaderGlobalBar,
  HeaderGlobalAction,
  SkipToContent,
} from 'carbon-components-react';
import { OverflowMenuVertical32 } from '@carbon/icons-react';
import {
  SixAtomicSymbolBlack,
  SynthesisHorizontalBlack,
} from '../../icons/CommonSvg';

const HeaderBar = () => (
  <Header aria-label="Header Bar">
    <SkipToContent />
    <div className="header-bar__container">
      <a href="https://www.sixatomic.com/">
        <SixAtomicSymbolBlack />
      </a>
      <div className="divider" />
      <div className="synthesis-svg" href="/">
        <a href="https://www.sixatomic.com/technology/synthesis">
          <SynthesisHorizontalBlack />
        </a>
      </div>
    </div>

    <HeaderNavigation aria-label="Patterns">
      <HeaderMenuItem className="header-menu-item">
        <div className="header-menu-item-text">Pattern</div>
      </HeaderMenuItem>
    </HeaderNavigation>
    <HeaderGlobalBar>
      <HeaderGlobalAction aria-label="menu-vertical">
        <OverflowMenuVertical32 />
      </HeaderGlobalAction>
    </HeaderGlobalBar>
  </Header>
);
export default HeaderBar;
