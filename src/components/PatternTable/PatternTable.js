import React from 'react';
import dayjs from 'dayjs';
import {
  DataTable,
  TableContainer,
  Table,
  TableHead,
  TableRow,
  TableHeader,
  TableBody,
  TableCell,
  Button,
  TableToolbar,
  TableToolbarContent,
  TableToolbarSearch,
  OverflowMenu,
  OverflowMenuItem,
  Link,
} from 'carbon-components-react';

import {
  Launch16,
  Copy16,
  Edit16,
  Checkmark16,
  Warning16,
  Add16,
} from '@carbon/icons-react';

const DateFormat = 'DD MMM YYYY HH:mm';
const PatternTable = ({ rowsData, headersData }) => {
  const Duplicate = () => (
    <div className="duplicate--icon">
      <div className="icon bx--overflow-menu__icon">
        <Copy16 />
      </div>
      <div className="text">Duplicate</div>
    </div>
  );

  const EditNote = () => (
    <div className="edit-note--icon">
      <div className="icon bx--overflow-menu__icon">
        <Edit16 />
      </div>
      <div className="text">Edit notes</div>
    </div>
  );

  const DetailCell = () => (
    <div className="cell-action__div">
      <Link href="#/detail">
        <div className="cell-action-detail__div">
          <div className="text">Details</div>
          <div className="icon">
            <Launch16 />
          </div>
        </div>
      </Link>
    </div>
  );

  const OverflowMenuCell = () => (
    <div className="cell-action-dropdown__div">
      <OverflowMenu
        className=" overflow-menu__container"
        iconClass="lunch16"
        direction="bottom"
        flipped>
        <OverflowMenuItem itemText={<Duplicate />} />
        <OverflowMenuItem itemText={<EditNote />} />
      </OverflowMenu>
    </div>
  );

  const RenderStatus = ({ value }) =>
    value ? (
      <span>
        <Checkmark16 />
      </span>
    ) : (
      <span className="icon-warning">
        <Warning16 />
      </span>
    );

  const convertSize = value => {
    if (value < 28) {
      return 'XS';
    } else if (value < 31) {
      return 'S';
    } else if (value < 34) {
      return 'M';
    } else if (value < 37) {
      return 'L';
    } else if (value < 40) {
      return 'XL';
    } else {
      return 'XXL';
    }
  };

  const renderCell = (cell, header) => {
    const { value } = cell;
    const { key } = header;
    if (key === 'detail') {
      return <DetailCell cell={cell} />;
    } else if (key === 'menu') {
      return <OverflowMenuCell />;
    } else if (key === 'status') {
      return <RenderStatus value={value} />;
    } else if (key === 'dateCreated') {
      const date = new Date(value);
      const format = dayjs(date).format(DateFormat);
      return format;
    } else if (key === 'size') {
      return convertSize(value);
    } else {
      return cell.value;
    }
  };

  return (
    <DataTable
      className="pattern-data-table"
      isSortable
      rows={rowsData}
      headers={headersData}
      useStaticWidth
      render={({ rows, headers, getHeaderProps, getTableProps }) => (
        <TableContainer className="pattern-table__container" title="Patterns">
          <TableToolbar arial-label="toolbar">
            <TableToolbarContent>
              <TableToolbarSearch
                className="toolbar--search"
                placeHolderText="Filter table"
              />
              <Button
                renderIcon={Add16}
                iconDescription="Add"
                kind="primary"
                className="create-pattern-btn">
                <div className="text">Create pattern</div>
              </Button>
            </TableToolbarContent>
          </TableToolbar>
          <Table {...getTableProps()} className="pattern-table">
            <TableHead className="table--head">
              <TableRow className="table--head_row">
                {headers.map(header => (
                  <TableHeader
                    {...getHeaderProps({ header })}
                    className={`w-${header.size}x`}>
                    {header.header}
                  </TableHeader>
                ))}
              </TableRow>
            </TableHead>
            <TableBody className="pattern--body">
              {rows.map(row => (
                <TableRow key={row.id} className="body--row">
                  {row.cells.map((cell, index) => (
                    <TableCell
                      key={cell.id}
                      id={cell.id}
                      className={`la-${cell.info.header} w-${headers[index].size}x`}>
                      <div className="label-div">
                        {renderCell(cell, headers[index])}
                      </div>
                    </TableCell>
                  ))}
                </TableRow>
              ))}
            </TableBody>
          </Table>
        </TableContainer>
      )}
    />
  );
};

export default PatternTable;
